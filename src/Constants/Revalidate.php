<?php

namespace Drupal\nextjs_revalidate\Constants;

final class Revalidate {

  public const NEXTJS_REVALIDATE_TOKEN_ENV = 'NEXTJS_REVALIDATE_TOKEN';

  public const DRUPAL_FRONT_URL_ENV = 'DRUPAL_FRONT_URL';

}
