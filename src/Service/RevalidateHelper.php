<?php

namespace Drupal\nextjs_revalidate\Service;

use Drupal\nextjs_revalidate\Constants\Revalidate;

class RevalidateHelper {

  /**
   * Will call the endpoint from next.js API to revalidate the needed tag.
   *
   * @param string $tag
   */
  public static function callRevalidate(string $tag): void {
    $front_url = getenv(Revalidate::DRUPAL_FRONT_URL_ENV);

    if (!$front_url) {
      return;
    }

    $client = new \GuzzleHttp\Client(
      [
        'base_uri' => getenv(Revalidate::DRUPAL_FRONT_URL_ENV),
      ]
    );

    try {
      $response = $client->get(
        '/api/revalidate/tag',
        [
          'query' => [
            'tag' => $tag,
            'token' => getenv(Revalidate::NEXTJS_REVALIDATE_TOKEN_ENV),
          ],
        ]
      );
      \Drupal::logger('nextjs_revalidate')->info(
        $response->getBody()->getContents()
      );
    } catch (\Exception $e) {
      \Drupal::logger('nextjs_revalidate')->error(
        getenv('DRUPAL_FRONT_URL') . ' | ' . $e->getMessage() . '|' . $tag
      );
    }
  }

}
