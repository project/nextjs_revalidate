<?php

namespace Drupal\nextjs_revalidate\Plugin\QueueWorker;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\nextjs_revalidate\Service\RevalidateHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Next.js revalidation Queue Worker.
 *
 * @QueueWorker(
 *   id = "nextjs_revalidate_queue",
 *   title = @Translation("Revalidate nextjs Queue"),
 *   cron = {"time" = 15}
 * )
 */
final class NextjsRevalidateQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Main constructor.
   *
   * @param array $configuration
   *   Configuration array.
   * @param mixed $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The connection to the database.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $database
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
  }

  /**
   * Used to grab functionality from the container.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   * @param array $configuration
   *   Configuration array.
   * @param mixed $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('database'),
    );
  }

  /**
   * Process the entity and call the revalidate endpoint on the next.js server.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $data
   *
   * @return void
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function processItem(mixed $data) {
    $path = $data->toUrl()->toString();

    //@TODO find a way to be able to alter these supported types.
    switch ($data->getEntityTypeId()) {
      case 'node':
        $tag = 'node:' . strtok($path, '?');
        break;
      case 'group':
        $id = $data->hasField('field_group_id') ?
          $data->get('field_group_id')->value :
          $data->id();

        $tag = 'group:' . $data->language()->getId() . ':' . $id;
        break;
      case 'menu':
        $tag = 'menu:' . $data->language()->getId() . ':' . $data->getMenuName();
        break;
      default:
        $tag = $data->getEntityTypeId() . ':' . $data->language()->getId() . ':' . $data->id();
    }

    RevalidateHelper::callRevalidate($tag);
  }

}
