# Next.js revalidate module
## Description

This module works only for an application that has next.js as front-end framework. The goal is to inform next server to invalidate the cache and revalidate the page.

## How-to
- composer require drupal/nextjs_revalidate
- drush en nextjs_revalidate -y
- Implement the `hook_nextjs_revalidate_entity($entity)` that makes the logic when an entity has been saved ()

_Example_
>     if (
>       !$entity instanceof \Drupal\node\NodeInterface &&
>       !$entity instanceof \Drupal\menu_link_content\MenuLinkContentInterface
>     ) {
>       return;
>     }
>     \Drupal::messenger()->addStatus(
>       t(
>         'Your change will be visible on the site in a few seconds.',
>         [],
>         ['context' => 'nextjs_revalidate']
>       )
>     );
>     $queue = \Drupal::service('queue')->get('nextjs_revalidate_queue');
>     $queue->createItem($entity);
- Do not forget to setup the drupal core:cron to purge the revalidation queue.
